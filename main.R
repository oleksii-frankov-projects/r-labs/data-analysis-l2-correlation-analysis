library(corrplot)
library(qgraph)
library(ggpubr)
library(ppcor)
library(outliers)

#Завантажуємо дані
data_load <- read.csv("data.csv")
variables <- c('Time.on.App', 'Time.on.Website',
               'Length.of.Membership', 'Yearly.Amount.Spent')
data <- data_load[variables]

#Дослідження Yearly.Amount.Spent
hist(data$Yearly.Amount.Spent, breaks = 30,
     main = "Гістограма для Yearly.Amount.Spent", xlab = "Значення",
     ylab = "Частота")

#Кількість пропущених значень
sum(is.na(data$Yearly.Amount.Spent))

#Аномалії
grubbs.test(data$Yearly.Amount.Spent)
grubbs.test(data$Yearly.Amount.Spent, opposite = TRUE)

#Q_Q
ggqqplot(data$Yearly.Amount.Spent, main = "Q-Q діаграма для Yearly.Amount.Spent")

#Парні кореляції
correlations <-cor(data[variables]) 
correlations

#p-values
cor.mtest(data[variables])$p

#Карта кореляцій
corrplot.mixed(correlations)

#Граф кореляцій
qgraph(correlations, nodeNames = variables, negDashed=TRUE)

#Частинні коефіцієнти кореляції
pcor(data)$estimate 

#p-values
pcor(data)$p.value

#Карта частинних кореляцій
corrplot.mixed(pcor(data)$estimate)

#Граф частинних кореляцій
qgraph(pcor(data)$estimate, nodeNames = variables, negDashed=TRUE)

#Множинні коефіцієнти детермінації

#Time.on.App
summary(lm(Time.on.App ~ Time.on.Website +
              Length.of.Membership + Yearly.Amount.Spent,data = data))

#Time.on.Website
summary(lm(Time.on.Website ~ Time.on.App +
             Length.of.Membership + Yearly.Amount.Spent,data = data))

#Length.of.Membership
summary(lm(Length.of.Membership ~ Time.on.App +
             Time.on.Website + Yearly.Amount.Spent,data = data))

#Yearly.Amount.Spent
summary(lm(Yearly.Amount.Spent ~ Time.on.App +
             Time.on.Website + Length.of.Membership,data = data))


